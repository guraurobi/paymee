<img src="https://www.paymee.io/img/githublogo.png" alt="Paymee" width="300px">

Paymee is a payment solution for everyone. 

Since there are billions of devices in this world, used daily by each one of us to create virtual content, 
[Paymee](https://paymee.io) comes as a monetization platform for your content, a place where you can sell what you create. 

Use Paymee to enable customers to purchase your goods in a secure, timely and efficient manner!

# Fitbit Integration

You can use [Paymee](https://paymee.io) as a payment solution for any watchface or app created for:
* Fitbit Versa Lite
* Fitbit Versa
* Fitbit Versa 2
* Fitbit Ionic

# Integration Steps

Follow the next steps to integrate the payment solution to your app:

## 1. Create a new product
[Login](https://paymee.io/login) or [register](https://paymee.io/register) an account with [Paymee](https://paymee.io) and add a new product.

## 2. Get the product UID
Click on the new added product from the list, then click on UID.

## 3. Integrate in fitbit app/watchface
The following files from your app or watchface need to be updated:

* app/index.js

```
Update file. Import paymee.js and start the payment process.

import * as paymee from "./paymee";
paymee.startPayment();
```

* app/paymee.js

```
Device to Companion Communication File
Copy the whole document in you application or watchface.
```

* companion/index.js

```
Companion to Paymee Server Communication File
Copy the whole document and replace 'YOUR_PRODUCT_ID' at the beggining of the file.

const PRODUCT_ID = "YOUR_PRODUCT_UID";
```

* resources/paymee.gui 

```
Paymee UI File. Copy the whole document in you application or watchface.
```

* resources/index.gui

```
Update file. Add the paymee tag at the end of your index.gui file.

<use href="#paymee" />
```

* resources/widgets.gui 

```
Update file. Import the paymee.gui file so you can use the tag.

<link rel="import" href="paymee.gui" />
```


# Contact

* Website: [https://paymee.io](https://paymee.io)
* Support email: [support@paymee.io](mailto:support@paymee.io)
